#include <stdio.h>

main() {
	/* exercise 1-6: verify value of getchar() != EOF */
	printf("%d\n", (getchar() != EOF));

	/* exercise 1-7: print value of EOF */
	printf("EOF = %d\n", EOF);
}
