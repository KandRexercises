#include <stdio.h>
/* exercise 1-12: print input, one word per line */
/* general idea: 
	accept input line
	print newline if whitespace is encountered
	continue reading input, but don't copy to stdout
	begin copying again on next line after reading non-whitespace chars
	stop once EOF is read
*/
main() {
	int c;

	while((c = getchar()) != EOF) {
		
		if(c == ' ' || c == '\t' || c == '\n') { 
			putchar('\n');
			while(c == ' ' || c == '\t' || c == '\n') 
				c = getchar();
		}
		putchar(c);

	}
	putchar('\n');
}
