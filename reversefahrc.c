#include <stdio.h> 
   /* exercise 1-5 */ 
   /* print Fahrenheit-Celsius table 
       for fahr = 300, 280, ..., 0; floating-point version */ 
  main() 
  { 

    	int fahr;
 
	printf("Fahr\tC\n"); 
	for (fahr = 300; fahr > 0; fahr -= 20)
		printf("%3.0d %6.1f\n", fahr, (fahr-32)*(5.0/9.0));
   
  }
