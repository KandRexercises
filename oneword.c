#include <stdio.h>
/* exercise 1-12: print the first word on each line */
/* general idea: 
	accept input line
	print up to first word, once a space is read stop for that line
	begin reading again on next line
	stop once EOF is read
	possibly putchar after all input is read
*/
main() {
	int c;
	int oword = 0;

	while((c = getchar()) != EOF) {
		
/*		if(c == ' ' && oword == 0) { */
		if(c == ' ') { 
			oword = 1;
			
		} else {
			if(oword == 0) {
				putchar(c);
			} else if(c == '\n') {
				oword = 0;
				putchar('\n');
			} 
			
		}

	}
	putchar('\n');
}
