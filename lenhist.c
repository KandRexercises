#include <stdio.h>

/* exercise 1-13: create a histogram of the length of words in the input */
/* count the lengths of the words w/ an iterator 
upon reading whitespace, increment the value saved at an array index equal to the iterator's value 
reset iterator to 0 */

main()
{
	int c, i, j, len;
	int nlen[100];

	len = 0;
	for(i = 0; i < 100; ++i) {
		nlen[i] = 0;
	}

	while((c = getchar()) != EOF) {
		if(c == ' ' || c == '\n' || c == '\t') {
			++nlen[len];
			len = 0;
		} else {
			len++;
		}
	}
	
	for (i = 1; i < 100; ++i) { 
		if (nlen[i] > 0)
		{
			printf("%d: ", i);
			for (j = 0; j < nlen[i]; ++j) {
				printf("*");
			}
			printf("\n");
		}
	}
}
