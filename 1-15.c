#include <stdio.h> 
   /* exercise 1-15: exercise 1-3 but with a function */ 
   /* print Fahrenheit-Celsius table 
       for fahr = 0, 20, ..., 300; floating-point version */ 
int fahrc(float lower, float upper, float step)
{
	float i;

	printf("Fahr\tC\n");	 
	for (i = lower; i <=upper; i += step)
	        printf("%3.0f %6.1f\n", i, ((5.0/9.0) * (i-32.0))); 

	return 0;		
}
  
main() 
{ 
    float fahr, celsius; 
    float lower, upper, step; 

    lower = 0;      /* lower limit of temperatuire scale */ 
    upper = 300;    /* upper limit */ 
    step = 20;      /* step size */ 
	fahrc(lower, upper, step);	

    /*fahr = lower; 
	printf("Fahr\tC\n");	 
    while (fahr <= upper) { 
        celsius = (5.0/9.0) * (fahr-32.0); 
        printf("%3.0f %6.1f\n", fahr, celsius); 
        fahr = fahr + step; 
    }*/ 
		
}
