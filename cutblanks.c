#include <stdio.h>
/* exercise 1-9: truncate a string of blanks to 1 blank */

main() {
	int c;
	int blank = 0;
	while((c = getchar()) != EOF) {
		if(c == ' ') {
			blank = 1;
		} else {
			if(blank == 1) {
				putchar(' ');
				blank = 0;
			}
			putchar(c);
		}
	}
	putchar('\n');
}
